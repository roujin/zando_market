from flask_wtf import FlaskForm
from wtforms import SelectField, SubmitField, StringField, IntegerField, validators, PasswordField
from wtforms.validators import DataRequired, EqualTo, Email
from wtforms_alchemy.fields import PhoneNumberField, CountryField

from .models import Utilisateur

class InscriptionForm(FlaskForm):
    nom = StringField('Nom', validators=[DataRequired()])
    prenom = StringField('Prénom', validators=[DataRequired()])
    status = SelectField('Status', choices=Utilisateur.STATUS)
    numero = PhoneNumberField('Téléphone', validators=[DataRequired()], region='CG', display_format='national')
    ville = CountryField('Pays')
    adresse = StringField('Adresse', validators=[DataRequired()])
    mot_de_passe = PasswordField('Mot de passe', validators=[DataRequired()])
    confirmation = PasswordField('Confirmation', validators=[EqualTo('mot_de_passe', message='mot de passe non identique')])
    valider = SubmitField('Inscription')


class ConnexionForm(FlaskForm):
    nom = StringField('Nom', validators=[DataRequired()])
    mot_de_passe = PasswordField('Mot de passe', validators=[DataRequired()])
    valider = SubmitField('Connexion')