from enum import unique
from sqlalchemy.orm import backref

from . import db

from flask_login import UserMixin

from sqlalchemy_utils import ChoiceType, PhoneNumberType, CountryType, PasswordType

from werkzeug.security import generate_password_hash, check_password_hash


class Utilisateur(UserMixin, db.Model):
    __tablename__ = "utilisateur"

    STATUS = [
    ('blank','----------'),
    ('vendeur', 'Vendeur'),
    ('client', 'Client')
]
    id = db.Column(db.Integer, primary_key=True)
    nom = db.Column(db.String(64))
    prenom = db.Column(db.String(64))
    status = db.Column(ChoiceType(STATUS), default='blank')
    numero = db.Column(PhoneNumberType(region='CG'))
    adresse = db.Column(db.String(64))
    ville = db.Column(CountryType())
    #mot_de_passe = db.Column(PasswordType(
       # schemes=['pbkdf2_sha512']
    #))
    password = db.Column(db.String(254))
    boutique = db.relationship('Boutique', backref='utilisateur', uselist=False)

    def password_hash(self, password):
        self.password = generate_password_hash(password, method="sha256")

    def check_password(self, password):
        return check_password_hash(self.password, password)


    def __repr__(self):
        return '<utilisateur: {}>'.format(self.nom)



class Boutique(db.Model):

    CATEGORIE = [
    ('blank', '-----------------------'),
    ('electromenager', 'Eletromenager'),
    ('bricole', 'Bricole'),
    ('accessoire', 'Accessoire'),
    ('bijoux et colier', 'Bijoux et Colier'),
    ('telephone', 'Téléphone'),
    ('informatique', 'Matériel Informatique'),
    ('gadget', 'Gadgets'),
    ('consommable', 'Consommable Informatique'),
    ('beaute et soins', 'Beauté et Soin corporel')
]
    id = db.Column(db.Integer, primary_key=True)
    nom_boutique = db.Column(db.String(120), nullable=False)
    categorie = db.Column(ChoiceType(CATEGORIE), default='blank')
    adresse = db.Column(db.String(120), nullable=True)
    telephone = db.Column(PhoneNumberType(region='CG'))
    utilisateur_id = db.Column(db.Integer, db.ForeignKey('utilisateur.id'), unique=True)

    def __repr__(self):
        return '<boutique>: {}>'.format(self.nom_boutique)


class Article(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    nom_article = db.Column(db.String(120), nullable=False)
    prix = db.Column(db.String(30), nullable=False)
    quantite = db.Column(db.Integer, nullable=False)

    def __repr__(self):
        return '<article: {}>'.format(self.nom_article)


