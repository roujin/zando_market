from flask import Blueprint
from flask import current_app as app
from flask import render_template, redirect, url_for, flash

from..forms import InscriptionForm, ConnexionForm

from ..models import Utilisateur, db

from flask_login import login_required,logout_user, current_user, login_user

from .. import login_manager    


#=============== Configuration de Blueprint ================#

home_bp = Blueprint(
    'home_bp', __name__,
    template_folder='templates',
    static_folder='static',
    static_url_path='/home/static'
) 

#=============== Zone des itineraires ====================#

@login_manager.user_loader
def load_user(user_id):
    """Check if user is logged-in on every page load."""
    if user_id is not None:
        return Utilisateur.query.get(user_id)
    return None

@home_bp.route('/')
def accueil():
    return render_template(
        'accueil.html'
    )

@home_bp.route('/inscription', methods=['GET', 'POST'])
def page_inscription():
    form = InscriptionForm()
    if form.validate_on_submit():
        deja_inscrit = Utilisateur.query.filter_by(nom=form.nom.data).first()
        if deja_inscrit is None:
            utilisateur = Utilisateur(
                nom=form.nom.data,
                prenom=form.prenom.data,
                status=form.status.data,
                numero=form.numero.data,
                adresse=form.adresse.data,
                ville=form.ville.data,
            )
            utilisateur.password_hash(form.mot_de_passe.data)
            db.session.add(utilisateur)
            db.session.commit()
            flash('votre enregistrement a été effectué avec success')
            return redirect(url_for('home_bp.page_connexion'))
        flash("Désolé ce nom d'utilisateur existe déjà !!")
    return render_template(
        'inscription.html',
        form=form
    )

@home_bp.route('/connexion', methods=["GET", "POST"])
def page_connexion():
    form = ConnexionForm()
    if form.validate_on_submit():
        user = Utilisateur.query.filter_by(nom=form.nom.data).first()
        if user and user.check_password(password=form.mot_de_passe.data):
            login_user(user)
            return redirect(url_for('home_bp.accueil'))
        return redirect(url_for('home_bp.page_connexion'))
    return render_template('connexion.html', form=form)


@home_bp.route('/deconnexion')
@login_required
def page_deconnexion():
    logout_user()
    return redirect(url_for('home_bp.accueil'))
