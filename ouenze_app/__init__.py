from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_login import LoginManager

db = SQLAlchemy()
migrate = Migrate()
login_manager = LoginManager()

def init_app():
    app = Flask(__name__, instance_relative_config=False)
    app.config.from_object('config.Config')

#================= Zone d'initialisation des plugins ================#
    db.init_app(app)
    migrate.init_app(app, db)
    login_manager.init_app(app)
    
#================= Zone d'importation des parties de l'application ======================#
    with app.app_context():

        from . import models

        #===== Enregistrement des plans dans l'app ======#
        from .home.routes import home_bp
        from .boutique.routes import boutique_bp

        app.register_blueprint(home_bp)
        app.register_blueprint(boutique_bp)
        
        #====== creation tables dans la base de donnée ======#
        db.create_all()

        return app

