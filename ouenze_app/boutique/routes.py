from flask import Blueprint
from flask import redirect, render_template, url_for


boutique_bp = Blueprint(
    'boutique_bp', __name__,
    static_folder='static',
    template_folder='templates',
    url_prefix='/boutique',
    static_url_path='boutique/static'
)

@boutique_bp.route('/accueil')
def accueil():
    return render_template('boutique/accueil.html')