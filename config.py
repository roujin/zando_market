from os import path, environ
from dotenv import load_dotenv

basedir = path.abspath(path.basename(__file__))
load_dotenv(path.join(basedir, '.env'))

class Config:
#=============== Variables d'environement =================#

    FLASK_ENV = environ.get("FLASK_ENV")
    FLASK_APP = environ.get("FLASK_APP")
    SECRET_KEY = environ.get("SECRET_KEY")
    DEBUG = True
    TEST = True

#=============== Variables du modèle et des fichiers statics =================#

    STATIC_FOLDER = 'static'
    TEMPLATE_FOLDER = 'templates'

#================ Variables de configuration de la base de donnée ==========#

    SQLALCHEMY_DATABASE_URI = environ.get("SQLALCHEMY_DATABASE_URI")
    SQLALCHEMY_ECHO = False
    SQLALCHEMY_TRACK_MODIFICATIONS = False